#!/usr/bin/python3

infix = ' comments were found for this channel.'
biggestCommentsCount = 0

with open('nohup.out') as f:
    lines = f.read().splitlines()
    for line in lines:
        if infix in line:
            #print(line)
            commentsCount = int(line.split(': ')[-1].split(infix)[0])
            #print(commentsCount)
            if biggestCommentsCount < commentsCount:
                biggestCommentsCount = commentsCount

print(biggestCommentsCount)
