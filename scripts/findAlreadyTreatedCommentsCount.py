#!/usr/bin/python3

PREFIX = 'Channels per second: '
alreadyTreatedCommentsCount = 0

with open('nohup.out') as f:
    lines = f.read().splitlines()
    for line in lines:
        if PREFIX in line:
            alreadyTreatedCommentsCount += int(line.split(PREFIX)[-1])
        #if 'UCsT0YIqwnpJCM-mx7-gSA4Q' in line:
        #    break

print(alreadyTreatedCommentsCount)
