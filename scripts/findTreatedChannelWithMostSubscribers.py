#!/usr/bin/python3

import os, requests

channelIds = [channelId.replace('.zip', '') for channelId in next(os.walk('channels/'))[2]]
maxResults = 50

channelIdsChunks = [channelIds[i : i + maxResults] for i in range(0, len(channelIds), maxResults)]
mostSubscriberCount = 0
mostSubscriberChannel = None

for channelIds in channelIdsChunks:
    url = 'https://yt.lemnoslife.com/noKey/channels?part=statistics&id=' + ','.join(channelIds)
    data = requests.get(url).json()
    items = data['items']
    for item in items:
        subscriberCount = int(item['statistics']['subscriberCount'])
        if mostSubscriberCount < subscriberCount:
            mostSubscriberCount = subscriberCount
            mostSubscriberChannel = item['id']

print(mostSubscriberChannel, mostSubscriberCount)
