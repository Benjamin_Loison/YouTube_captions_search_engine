#!/usr/bin/python3

# We can't proceed automatically by using `requests` Python module because https://socialblade.com/youtube/top/country/fr/mostsubscribed is protected by CloudFlare.
# Note that `undetected-chromedriver` might be a workaround this limitation.

with open('mostsubscribed.html') as f:
    lines = f.read().splitlines()

PREFIX = '                   <a href = "/youtube/channel/'

for line in lines:
    if PREFIX in line:
        channelId = line.split(PREFIX)[1].split('">')[0]
        print(channelId)
