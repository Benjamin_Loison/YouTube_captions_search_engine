#!/usr/bin/python3

import shutil, os

infix = ': Treating channel '
path = 'channels/'

threads = {}

with open('nohup.out') as f:
    lines = f.read().splitlines()
    for line in lines:
        if infix in line:
            #print(line)
            threadId = line.split(': ')[1]
            channelId = line.split(infix)[1].split(' (')[0]
            if threadId.isdigit() and channelId.startswith('UC') and len(channelId) == 24:
                threads[threadId] = channelId
    for threadId in threads:
        channelId = threads[threadId]
        print(threadId, channelId)
        # There are three cases:
        # - `channelId`/ exists
        # - `channelId`/ and `channelId`.zip exist
        # - `channelId`.zip exists
        # To manage every case, we need to use two `try`/`except`.
        try:
            shutil.rmtree(path + channelId)
        except:
            pass
        try:
            os.remove(path + channelId + ".zip")
        except:
            pass
