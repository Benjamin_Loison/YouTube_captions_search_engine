<?php

    if (!function_exists('str_contains')) {
        function str_contains($haystack, $needle)
        {
            return strpos($haystack, $needle) !== false;
        }
    }

    if (!function_exists('str_ends_with')) {
        function str_ends_with($haystack, $needle)
        {
            $length = strlen($needle);
            return $length > 0 ? substr($haystack, -$length) === $needle : true;
        }
    }

    function str_replace_first($needle, $replace, $haystack) {
        $pos = strpos($haystack, $needle);
        if ($pos !== false) {
            $haystack = substr_replace($haystack, $replace, $pos, strlen($needle));
        }
        return $haystack;
    }

    $uri = $_SERVER['REQUEST_URI'];
    $uri = str_replace('/channels/', '', $uri);
    $prefix = '/mnt/HDD0/YouTube_captions_search_engine/channels/';
    if (str_contains($uri, '/')) {
        $uri = str_replace_first('/', '#', $uri);
        $uri = $prefix . $uri;
        if (str_ends_with($uri, '.json')) {
            header('Content-Type: application/json; charset=UTF-8');
        }
        echo file_get_contents("zip://$uri");
    } else {
        $uri = $prefix . $uri;
        header("Content-Type: application/zip");
        echo readfile($uri);
    }

?>
